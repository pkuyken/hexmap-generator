package main

import (
	"flag"
	"os"
	"strconv"

	svg "github.com/ajstarks/svgo/float"
)

// HexStrokeOpacity is the SVG stroke opacity for the hexagon borders
const HexStrokeOpacity string = "stroke-opacity='1.0'"

// HexStrokeColor is the SVG stroke color for the hexagon borders
const HexStrokeColor string = "darkgrey"

// LineStrokeColor is the SVG stroke color for the inner grid lines
const LineStrokeColor string = "stroke='darkslategrey'"

// LineStrokeLinecap is the SVG stroke linecap for the grid lines
const LineStrokeLinecap string = "stroke-linecap=\"butt\""

// BorderStrokeColor is the SVG stroke color for the inner grid lines
const BorderStrokeColor string = "black"

func main() {

	leftMargin := flag.Int("leftMargin", 40, "map page left margin")
	topMargin := flag.Int("topMargin", 75, "map page top margin")
	hexHeight := flag.Int("hexHeight", 24, "height of each hex")
	rows := flag.Int("rows", 36, "number of rows in the hex grid")
	columns := flag.Int("cols", 43, "number of columns in the hex grid")

	flag.Parse()

	ri := float64(*hexHeight) / 2.0
	columnCount := *columns
	rowCount := *rows
	leftOffset := float64(*leftMargin)
	topOffset := float64(*topMargin)
	viewOffset := Point{leftOffset, topOffset}
	edgeStyle := FlatGridEdge
	gridStyle := EvenQ

	bottomLeftHex := HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, rowCount+1, 1)
	bottomLeft := calcPointE(bottomLeftHex, ri)
	bottomLeft.X = bottomLeftHex.X

	bottomRightHex := HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, rowCount+1, columnCount)
	bottomRight := calcPointE(bottomRightHex, ri)
	bottomRight.X = bottomRightHex.X

	topRightHex := HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, 1, columnCount)
	topRight := calcPointE(topRightHex, ri)
	topRight.X = topRightHex.X

	var rectPoints []Point
	rectPoints = append(rectPoints, viewOffset)
	rectPoints = append(rectPoints, bottomLeft)
	rectPoints = append(rectPoints, bottomRight)
	rectPoints = append(rectPoints, topRight)

	rectX, rectY := verticesToCoordinateArrays(rectPoints)
	width := bottomRight.X + viewOffset.X
	height := bottomRight.Y + viewOffset.Y + 100.0

	var polygons [][]Point

	for row := 1; row <= rowCount; row++ {

		for col := 1; col <= columnCount; col++ {
			center := HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, row, col)

			if edgeStyle == FlatGridEdge && gridStyle == EvenQ {
				if col == 1 {
					polygons = append(polygons, calcLeftSideHalfHex(center, ri))
				} else if col == columnCount && columnCount%2 == 1 {
					polygons = append(polygons, calcRightSideHalfHex(center, ri))
				} else if row == 1 && col%2 == 0 {
					polygons = append(polygons, calcTopHalfHex(gridStyle, center, ri))
				} else if row == rowCount && col%2 == 0 {
					polygons = append(polygons, calcHexagon(center, ri))
					polygons = append(polygons, calcBottomHalfHex(gridStyle,
						HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, row+1, col), ri))
				} else {
					polygons = append(polygons, calcHexagon(center, ri))
				}
			} else {
				polygons = append(polygons, calcHexagon(center, ri))
			}
		}
	}

	canvas := svg.New(os.Stdout)
	// canvas.StartviewUnit(8.5, 11.0, "in", 0.0, 0.0, width, height)
	canvas.Start(width, height, "id=\"atlasHexGrid\"")
	canvas.Def()
	canvas.Style("text/css",
		`@import url('https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer');
	@font-face {
		font-family: 'IM Fell Great Primer', serif;
		font-weight: normal;
		font-style: normal;
	}

	polygon.hexStyle {
		stroke: darkgrey;
		fill-opacity: 0.0;
	}

	text.coordStyle {
		font-family: 'IM Fell Great Primer', serif;
		font-size: 24px;
		text-anchor: middle;
	}`)
	canvas.DefEnd()

	//canvas.Group("id=\"atlasHexGrid\"", "preserveAspectRatio=\"YMin\"")

	for _, polygon := range polygons {
		xValues, yValues := verticesToCoordinateArrays(polygon)

		canvas.Polygon(xValues, yValues, "class=\"hexStyle\"")
	}

	outerRectX := []float64{}
	for _, xCoord := range rectX {
		if xCoord == viewOffset.X {
			outerRectX = append(outerRectX, xCoord-25.0)
		} else {
			outerRectX = append(outerRectX, xCoord+25.0)
		}
	}
	outerRectY := []float64{}
	for _, yCoord := range rectY {
		if yCoord == viewOffset.Y {
			outerRectY = append(outerRectY, yCoord-25.0)
		} else {
			outerRectY = append(outerRectY, yCoord+25.0)
		}
	}

	// Border rectangle
	canvas.Polygon(rectX, rectY, "stroke=\""+BorderStrokeColor+"\"", "stroke-width='2'", "fill-opacity='0.0'")
	canvas.Polygon(outerRectX, outerRectY, "stroke=\""+BorderStrokeColor+"\"", "stroke-width='2'", "fill-opacity='0.0'")

	lastColLineX := viewOffset.X

	columnLabelY := []float64{}

	// Column Lines
	for colLineNum := 8; colLineNum <= 36; colLineNum += 7 {
		bottomLineHex := HexCenterFromCoordinates(gridStyle, edgeStyle, viewOffset, ri, rowCount, colLineNum)
		lineX := bottomLineHex.X
		lineY := bottomLeft.Y

		canvas.Line(lineX, viewOffset.Y,
			lineX, lineY,
			LineStrokeColor, LineStrokeLinecap, "stroke-width='1'")

		columnLabelY = append(columnLabelY, ((lastColLineX + lineX) / 2.0))
		lastColLineX = lineX

	}
	columnLabelY = append(columnLabelY, (lastColLineX+topRight.X)/2.0)

	columnLabel := 'A'
	for _, columnLabelYVal := range columnLabelY {
		canvas.Text(columnLabelYVal, viewOffset.Y-4.0, string(columnLabel), "class=\"coordStyle\"")
		canvas.Text(columnLabelYVal, bottomLeft.Y+21.0, string(columnLabel), "class=\"coordStyle\"")
		columnLabel++
	}

	// Row Lines
	lastRowY := viewOffset.Y
	rowLabelXCoords := []float64{}

	firstRowY := viewOffset.Y
	if rowCount > 6 {
		for rowLineNum := 1; rowLineNum < 6; rowLineNum++ {
			lineY := firstRowY + 12*ri*float64(rowLineNum)
			canvas.Line(viewOffset.X, lineY,
				bottomRight.X, lineY,
				LineStrokeColor, LineStrokeLinecap, "stroke-width='1'")

			rowLabelXCoords = append(rowLabelXCoords, (lineY+lastRowY)/2.0)
			lastRowY = lineY
		}
	}
	rowLabelXCoords = append(rowLabelXCoords, (lastRowY+bottomLeft.Y)/2.0)
	rowLabel := 1
	for _, rowLabelY := range rowLabelXCoords {
		canvas.Text(viewOffset.X-13.0, rowLabelY, strconv.Itoa(rowLabel), "class=\"coordStyle\"")
		canvas.Text(topRight.X+14.0, rowLabelY, strconv.Itoa(rowLabel), "class=\"coordStyle\"")
		rowLabel++
	}
	//	canvas.Gend()

	canvas.End()
}
