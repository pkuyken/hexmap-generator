package main

import (
	"fmt"
	"testing"
)

func TestHexCenterFromCoordinates(t *testing.T) {
	response := HexCenterFromCoordinates(EvenQ, PlainGridEdge, Point{100.0, 100.0}, 12, 0, 0)
	fmt.Printf("Center: %+v\n", response)
	// other stuff
}
