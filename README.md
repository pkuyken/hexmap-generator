# Hexmap Generator

## Goals

The Hexmap Generator creates blank templates suitable for Old School Revival (OSR)
styled tabletop roleplaying games.  The map styles are based on the amazing [Welsh Piper templates](https://www.welshpiper.com/hex-templates/).

Unlike the Welsh Piper templates which use a 5/25 mile hex standard, the Hexmap Generator
utilizes a 6/24 mile hex standard based around the [Adventurer Conqueror King System (ACKS)](http://autarch.co/)
roleplaying game.

## Design Notes

The first map template being developed is the Atlas Page template. It will generate a 43x36 half-hex bordered
area, suitable for use at the 1 hex = 24 miles high scale.

## Fonts

Currently leaning towards using the [IM Fell Great Primer](https://fonts.google.com/specimen/IM+Fell+Great+Primer)
font.

`font-family: 'IM Fell Great Primer', serif;`

## Technical Limitations

Currently there are no configurable options.

There is only support for an EvenQ based grid style (even columns are higher).

The map is indexed based on 1 being the first row/column, not 0.
