package main

import (
	"math"
)

// Point is a float64 based X/Y coordinate.
type Point struct {
	X, Y float64
}

// HexGridOpts describes the options for generating a hex grid.
type HexGridOpts struct {
	ViewOffset    Point
	GridStyle     HexGridStyle
	EdgeStyle     HexGridEdgeStyle
	Rows, Columns int
	LabelHexes    bool
}

// HexGridStyle is the grid layout style
type HexGridStyle int

const (
	// EvenQ hex grids start with the even numbered columns
	// (0, 2, 4, ...) lower than the odd numbered columns
	EvenQ HexGridStyle = 0

	// OddQ hex grids start with the odd numbered columns
	// (1, 3, 5, ...) lower than the even numbered columns
	OddQ HexGridStyle = 1
)

// HexGridEdgeStyle determines the style (flat or plain)
// of the generated hex grid
type HexGridEdgeStyle int

const (
	// PlainGridEdge sets the grid generation to output a
	// regular series of hexagons
	PlainGridEdge HexGridEdgeStyle = 0

	// FlatGridEdge configures the grid generation to output
	// half hexagons on the edges of the grid to make a smooth
	// border.
	FlatGridEdge HexGridEdgeStyle = 1
)

func calcPointA(center Point, ri float64) Point {
	pointA := Point{}
	pointA.X = center.X - RiToA(ri)
	pointA.Y = center.Y
	return pointA
}

func calcPointB(center Point, ri float64) Point {
	pointB := Point{}
	pointB.X = center.X - (RiToA(ri) / 2)
	pointB.Y = center.Y + ri
	return pointB
}

func calcPointC(center Point, ri float64) Point {
	pointC := Point{}
	pointC.X = center.X + (RiToA(ri) / 2)
	pointC.Y = center.Y + ri
	return pointC
}

func calcPointD(center Point, ri float64) Point {
	pointD := Point{}
	pointD.X = center.X + RiToA(ri)
	pointD.Y = center.Y
	return pointD
}

func calcPointE(center Point, ri float64) Point {
	pointE := Point{}
	pointE.X = center.X + (RiToA(ri) / 2)
	pointE.Y = center.Y - ri
	return pointE
}

func calcPointF(center Point, ri float64) Point {
	pointF := Point{}
	pointF.X = center.X - (RiToA(ri) / 2)
	pointF.Y = center.Y - ri
	return pointF
}

func calcHexagon(center Point, ri float64) (vertices []Point) {
	vertices = append(vertices, calcPointA(center, ri))
	vertices = append(vertices, calcPointB(center, ri))
	vertices = append(vertices, calcPointC(center, ri))
	vertices = append(vertices, calcPointD(center, ri))
	vertices = append(vertices, calcPointE(center, ri))
	vertices = append(vertices, calcPointF(center, ri))

	return vertices
}

func calcLeftSideHalfHex(center Point, ri float64) (vertices []Point) {
	vertexB := calcPointB(center, ri)
	vertexB.X = vertexB.X + 0.5*RiToA(ri)
	vertexF := calcPointF(center, ri)
	vertexF.X = vertexF.X + 0.5*RiToA(ri)

	vertices = append(vertices, vertexB)
	vertices = append(vertices, calcPointC(center, ri))
	vertices = append(vertices, calcPointD(center, ri))
	vertices = append(vertices, calcPointE(center, ri))
	vertices = append(vertices, vertexF)

	return vertices
}

func calcRightSideHalfHex(center Point, ri float64) (vertices []Point) {
	vertexC := calcPointC(center, ri)
	vertexC.X = vertexC.X - 0.5*RiToA(ri)
	vertexE := calcPointE(center, ri)
	vertexE.X = vertexE.X - 0.5*RiToA(ri)

	vertices = append(vertices, vertexC)
	vertices = append(vertices, calcPointB(center, ri))
	vertices = append(vertices, calcPointA(center, ri))
	vertices = append(vertices, calcPointF(center, ri))
	vertices = append(vertices, vertexE)

	return vertices
}

func calcBottomHalfHex(gridStyle HexGridStyle, center Point, ri float64) (vertices []Point) {

	vertices = append(vertices, calcPointA(center, ri))
	vertices = append(vertices, calcPointD(center, ri))
	vertices = append(vertices, calcPointE(center, ri))
	vertices = append(vertices, calcPointF(center, ri))

	return vertices
}

func calcTopHalfHex(gridStyle HexGridStyle, center Point, ri float64) (vertices []Point) {

	vertices = append(vertices, calcPointA(center, ri))
	vertices = append(vertices, calcPointB(center, ri))
	vertices = append(vertices, calcPointC(center, ri))
	vertices = append(vertices, calcPointD(center, ri))

	return vertices
}

func verticesToCoordinateArrays(vertices []Point) (xValues []float64, yValues []float64) {
	for _, vertex := range vertices {
		xValues = append(xValues, vertex.X)
		yValues = append(yValues, vertex.Y)
	}

	return xValues, yValues
}

// HexCenterFromCoordinates calculates the center coordinates
// of a hexagon with the correct offset based on the starting
// vertex of the coordinate set.
func HexCenterFromCoordinates(gridStyle HexGridStyle, gridEdge HexGridEdgeStyle, viewOffset Point, ri float64, row int, col int) (center Point) {
	evenColumn := col%2 == 0
	a := RiToA(ri)

	switch gridEdge {
	case FlatGridEdge:
		viewOffset.X = viewOffset.X - a
		viewOffset.Y = viewOffset.Y - ri
	case PlainGridEdge:
		// do Nothing
	}

	switch gridStyle {
	case EvenQ:
		if evenColumn {
			center.X = viewOffset.X + a + (float64(col-1) * 1.5 * a)
			center.Y = viewOffset.Y - ri + (float64(row) * 2.0 * ri)
		} else {
			center.X = viewOffset.X - 0.5*a + (float64(col) * 1.5 * a)
			center.Y = viewOffset.Y + 2*ri + (float64(row-1) * 2 * ri)
		}

	case OddQ:
		if evenColumn {
			center.X = viewOffset.X - 0.5*a + (float64(col) * 1.5 * a)
			center.Y = viewOffset.Y + 2*ri + (float64(row-1) * 2 * ri)
		} else {
			center.X = viewOffset.X + a + (float64(col-1) * 1.5 * a)
			center.Y = viewOffset.Y - ri + (float64(row) * 2.0 * ri)
		}
	}

	return center
}

// RiToA converts the inscribe radius of a hexagon into the side length
// of the hexagon
func RiToA(inscribeRadius float64) float64 {
	return (2.0 * inscribeRadius) / math.Sqrt(3)
}
